import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
from pexels_api import API


# This works but it is using the API from pexel....
# Not actually using a header tag.


def get_city_pic(city, state):
    api = API(PEXELS_API_KEY)
    print(city)
    print(state)
    search = api.search(f"{state} {city}", page=1, results_per_page=1)
    photo_url = search["photos"][0]["src"]["medium"]
    return {"photo_url": photo_url}

    # actual way the project wanted to do.
    # def get_photo(city, state):
    # headers = {"Authorization": PEXELS_API_KEY}
    # params = {
    #     "per_page": 1,
    #     "query": f"{state} {city}",
    # }
    # url = "https://api.pexels.com/v1/search"
    # response = requests.get(url, params=params, header=header)
    # content = json.loads(response.content)
    # try:
    #   return {"picture_url": content['photos'][0]['src']['original']}
    # except:
    #   return {"picture_url": None}


def get_weather_data(city, state):
    params = {
        "q": f"{city}, {state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    data = json.loads(response.content)
    lat = data[0]["lat"]
    lon = data[0]["lon"]

    params = {
        "lat": lat,
        "lon": lon,
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    return {
        "temperature": content["main"]["temp"],
        "weather_description": content["weather"][0]["description"],
    }
