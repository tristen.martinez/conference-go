from django.http import JsonResponse
import json
from .models import AccountVO, Attendee
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from .models import ConferenceVO


# New encoder for ConferenceV0
class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
    ]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        "email",
        "company_name",
        "created",
        "conference",
    ]

    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }

    def get_extra_data(self, o):
        count = AccountVO.objects.filter(email=o.email).count()
        print(f"{count} this is the count")
        if count > 0:
            return {"has_account": True}
        return {"has_account": False}


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        print(attendees)
        print(f"this is conferencevoid: {conference_vo_id}")
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            conference_href = content["conference"]
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse({"message": "Invalid conference ID"})

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, pk):

    if request.method == "GET":
        try:
            attendee = Attendee.objects.get(id=pk)
            return JsonResponse(
                attendee,
                encoder=AttendeeDetailEncoder,
                safe=False,
            )
        except Attendee.DoesNotExist:
            return JsonResponse({"message": "This is an invlaid attendee ID"})
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            Attendee.objects.filter(id=pk).update(**content)

            attendee = Attendee.objects.get(id=pk)
            return JsonResponse(
                attendee, encoder=AttendeeDetailEncoder, safe=False
            )
        except Attendee.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Conference ID or Invalid attendee ID"}
            )
